"""
Processor operations simulation
"""
import os
import struct


class Processor:
	"""
	Processor
	1 addresses
	8 bits
	"""
	
	def __init__(self, commands_file=os.getcwd() + '/task.txt',
	             characteristic_bits_amount=7,
	             mantissa_bits_amount=6):
		# default file is commands.txt
		# IEEE754 format
		self.characteristic_bits_amount = characteristic_bits_amount
		self.characteristic_bits_bias = 2 ** characteristic_bits_amount // 2
		self.mantissa_bits_amount = mantissa_bits_amount
		
		self.executing_commands_file = commands_file
		self.commands = None
		
		self.register_1 = None
		self.register_2 = None
		self.register_3 = None
		self.register_4 = None
		self.register_5 = None
		self.register_6 = None
		self.register_7 = None
		self.register_8 = None
		
		self.max_bound = 2 ** self.characteristic_bits_amount - 1
		self.min_bound = - 2 ** self.characteristic_bits_amount
	
	def load_all_commands(self, file):
		"""
		Loading commands
		:param file: txt file for commands
		"""
		commands = []  # all commands for executing
		with open(file, 'r') as f:  # reading commands from file
			for line in f:  # check every line
				if line[-1] == '\n':  # skip last el or not
					commands.append(line[:-1])  # skip newline el '\n'
				else:
					commands.append(line)  # not skip last el
		print(commands)
		self.commands = commands
	
	@staticmethod
	def print_to_console(s, h, m, num):
		"""
		print represent of bin num
		"""
		return str(s) + ' ' + str(h) + ' ' + str(m) + ' Represents: ' + str(num)
	
	@staticmethod
	def invert_bits(num):
		"""
		Invert bits for negative num
		"""
		inv_n = ''
		for n in num:
			if n == '1':
				inv_n = inv_n + '0'
			if n == '0':
				inv_n = inv_n + '1'
		return inv_n
	
	@staticmethod
	def to_binary(num):
		"""
		To bin
		"""
		return ''.join('{:0>8b}'.format(c) for c in struct.pack('!f', num))
	
	# Python function to convert float
	# decimal to binary number
	# Function returns octal representation
	def decimal_to_ieee754(self, number):
		"""
		Float to bin convert
		"""
		if not number:
			return None, '', ''
		if number > self.max_bound:
			raise Exception(f'Too positive number {number} for upper bound with h {self.characteristic_bits_amount}:'
			                f' {self.max_bound} 2^(-1)*2**{self.characteristic_bits_amount} - 1')
		if number < self.min_bound:
			raise Exception(f'Too negative number {number} for upper bound with h {self.characteristic_bits_amount}:'
			                f' {self.max_bound} 2^(-1)*2**{self.characteristic_bits_amount}')
		# convert 1E-7 form or 1E+10form types
		number = str(number)
		if number[1] == 'e':
			if number[2] == '-':
				n = '0.' + '0' * (int(number[3:]) - 1) + '1'
				if number[0] == '-':
					number = '-' + n
				else:
					number = n
			if number[2] == '+':
				n = '1' + '0' * (int(number[3:])) + '.0'
				if number[0] == '-':
					number = '-' + n
				elif number[0] == '+':
					number = n
		# split() separates whole number and decimal
		# part and stores it in two separate variables
		whole, dec = number.split(".")
		h = 0
		# Convert both whole number and decimal
		# part from string type to integer type
		whole = int(whole)
		if whole < 1:
			for i, dig in enumerate(dec):  # num of 0 in dec
				if dig != '0':
					h = - i
					break
		dec = int(dec)
		# Convert the whole number part to its
		# respective binary form and remove the
		# "0b" from it.
		sign = '0'
		if whole < 0:
			sign = '1'
			whole = abs(whole)
		res = bin(whole).lstrip("0b")
		sc_res = res
		if whole > 1:
			h_ = len(res) - 1
		else:
			h_ = h
		
		# Iterate the number of times, we want
		# Function converts the value passed as
		# parameter to its decimal representation
		
		def decimal_converter(nu):
			"""
			Convert
			"""
			while nu >= 1:
				nu /= 10
			return nu
		
		for x in range(self.mantissa_bits_amount):  # the number of decimal places to be
			# Multiply the decimal value by 2 and separate the whole number part and decimal part
			wd = str(float(decimal_converter(int(dec)) * 2))
			if '.' in wd:
				whole, dec = wd.split(".")
			else:
				whole = int(wd)
				dec = '0'
			# Convert the decimal part to integer again
			dec = int(dec)
			# Keep adding the integer parts and receive to the result variable
			sc_res = sc_res + str(whole)  # scientific result
		ch = bin(self.characteristic_bits_bias + h_).lstrip('0b')  # characteristic bits
		ch = '0' * (self.characteristic_bits_amount - len(ch)) + ch  # characteristic bits
		if sign == '0':
			num = int(sc_res, 2) * 2 ** (h - self.mantissa_bits_amount)
			return sign, ch, sc_res[:self.mantissa_bits_amount], num, h
		if sign == '1':
			num = - int(sc_res, 2) * 2 ** (h - self.mantissa_bits_amount)
			return sign, ch, sc_res[:self.mantissa_bits_amount], num, h
	
	# operations
	def load(self, x):
		"""
		Load value to register
		:param x: value
		"""
		if x is not None:
			x = float(x)
			if self.min_bound < x < self.max_bound:
				return x
			else:
				raise Exception(f'{x} value not in bounds:'
				                f'{self.min_bound}, {self.max_bound}')
		elif x == 0 or '0':
			x = float(x)
			return self.decimal_to_ieee754(x)[3]
		else:
			return None
	
	def add(self, x, y):
		"""
		Logical adding 0+0->0 ; 0+1 or 1+0 -> 1; 1+1 -> 0 (+1 to next grade)
		:param x:
		:param y:
		:return:
		"""
		x = float(x)
		y = float(y)
		if self.min_bound < y < self.max_bound:
			nw = x + y
			nw = self.decimal_to_ieee754(nw)[3]
			if self.min_bound < nw < self.max_bound:
				return self.decimal_to_ieee754(nw)[3]
			else:
				raise Exception(f'{x} + {y} = {nw} value not in bounds:'
				                f'{self.min_bound}, {self.max_bound}')
		else:
			raise Exception(f'{y} value not in bounds:'
			                f'{self.min_bound}, {self.max_bound}')
	
	def sub(self, x, y):
		"""
		Binary subtraction 0-0->0 ; 0-1 (-1 to next grade) or 1-0 -> 1; 1-1 -> 0
		:param x:
		:param y:
		:return:
		"""
		x = float(x)
		y = float(y)
		if self.min_bound < y < self.max_bound:
			nw = x - y
			nw = self.decimal_to_ieee754(nw)[3]
			if self.min_bound < nw < self.max_bound:
				return self.decimal_to_ieee754(nw)[3]
			else:
				raise Exception(f'{x} - {y} = {nw} value not in bounds:'
				                f'{self.min_bound}, {self.max_bound}')
		else:
			raise Exception(f'{y} value not in bounds:'
			                f'{self.min_bound}, {self.max_bound}')
	
	def mult(self, x, y):
		"""
		Binary multiplying 0-0 -> 0 ; 0-1 ->0 or 1-0 -> 0; 1-1 -> 1
		:param x:
		:param y:
		:return:
		"""
		x = float(x)
		y = float(y)
		if self.min_bound < y < self.max_bound:
			nw = x * y
			nw = self.decimal_to_ieee754(nw)[3]
			if self.min_bound < nw < self.max_bound:
				return self.decimal_to_ieee754(nw)[3]
			else:
				raise Exception(f'{x} * {y} = {nw} value not in bounds:'
				                f'{self.min_bound}, {self.max_bound}')
		else:
			raise Exception(f'{y} value not in bounds:'
			                f'{self.min_bound}, {self.max_bound}')
	
	def div(self, x, y):
		"""
		Binary subtraction 0-0->0 ; 0-1 (-1 to next grade) or 1-0 -> 1; 1-1 -> 0
		:param x:
		:param y:
		:return:
		"""
		x = float(x)
		y = float(y)
		if self.min_bound < y < self.max_bound:
			nw = x / y
			nw = self.decimal_to_ieee754(nw)[3]
			if self.min_bound < nw < self.max_bound:
				return nw
			else:
				raise Exception(f'{x} / {y} = {nw} value not in bounds:'
				                f'{self.min_bound}, {self.max_bound}')
		else:
			raise Exception(f'{y} value not in bounds:'
			                f'{self.min_bound}, {self.max_bound}')
	
	def ex_com(self, current_command, cl):
		"""
		Executing single command
		:param current_command:
		:param cl: clock
		"""
		print(f"Command № {cl // 2 + 1} clock rate {cl + 1}\n"
		      f'Command: {current_command}')
		parts = [p for p in current_command.split()]
		current_command = parts[0]
		value = parts[1]
		register_1 = self.decimal_to_ieee754(self.register_1)
		register_2 = self.decimal_to_ieee754(self.register_2)
		register_3 = self.decimal_to_ieee754(self.register_3)
		register_4 = self.decimal_to_ieee754(self.register_4)
		register_5 = self.decimal_to_ieee754(self.register_5)
		register_6 = self.decimal_to_ieee754(self.register_6)
		register_7 = self.decimal_to_ieee754(self.register_7)
		register_8 = self.decimal_to_ieee754(self.register_8)
		print(f"1R: {register_1[0]} {register_1[1]} {register_1[2]}   {self.register_1}\n"
		      f"2R: {register_2[0]} {register_2[1]} {register_2[2]}   {self.register_2}\n"
		      f"3R: {register_3[0]} {register_3[1]} {register_3[2]}   {self.register_3}\n"
		      f"4R: {register_4[0]} {register_4[1]} {register_4[2]}   {self.register_4}\n"
		      f"5R: {register_5[0]} {register_5[1]} {register_5[2]}   {self.register_5}\n"
		      f"6R: {register_6[0]} {register_6[1]} {register_6[2]}   {self.register_6}\n"
		      f"7R: {register_7[0]} {register_7[1]} {register_7[2]}   {self.register_7}\n"
		      f"8R: {register_8[0]} {register_8[1]} {register_8[2]}   {self.register_8}\n"
		      '\n'
		      f"IC: {current_command}")
		print()
		if current_command == 'load':
			if 'R' not in value:  # load number
				self.register_1 = self.decimal_to_ieee754(float(value))[3]
			elif value == 'R2':  # load from another register
				self.register_1 = self.register_2
			elif value == 'R3':
				self.register_1 = self.register_3
			elif value == 'R4':
				self.register_1 = self.register_4
			elif value == 'R5':
				self.register_1 = self.register_5
			elif value == 'R6':
				self.register_1 = self.register_6
			elif value == 'R7':
				self.register_1 = self.register_7
			elif value == 'R8':
				self.register_1 = self.register_8
		elif current_command == 'copy':
			# copy from R1
			# Saving in another register (copy to)
			if value == 'R2':
				self.register_2 = self.register_1
			elif value == 'R3':
				self.register_3 = self.register_1
			elif value == 'R4':
				self.register_4 = self.register_1
			elif value == 'R5':
				self.register_5 = self.register_1
			elif value == 'R6':
				self.register_6 = self.register_1
			elif value == 'R7':
				self.register_7 = self.register_1
			elif value == 'R8':
				self.register_8 = self.register_1
			else:
				raise Exception(f'Unknown value {value} for copy')
		# add bin
		elif current_command == 'add':
			if value == 'R1':
				new_value = self.register_1
			elif value == 'R2':
				new_value = self.register_2
			elif value == 'R3':
				new_value = self.register_3
			elif value == 'R4':
				new_value = self.register_4
			elif value == 'R5':
				new_value = self.register_5
			elif value == 'R6':
				new_value = self.register_6
			elif value == 'R7':
				new_value = self.register_7
			elif value == 'R8':
				new_value = self.register_8
			else:
				raise Exception(f'Unknown value {value} for add')
			# add
			new_value = self.add(x=self.register_1, y=new_value)
			# save
			self.register_1 = self.decimal_to_ieee754(float(new_value))[3]
		elif current_command == 'sub':
			if value == 'R1':
				new_value = self.register_1
			elif value == 'R2':
				new_value = self.register_2
			elif value == 'R3':
				new_value = self.register_3
			elif value == 'R4':
				new_value = self.register_4
			elif value == 'R5':
				new_value = self.register_5
			elif value == 'R6':
				new_value = self.register_6
			elif value == 'R7':
				new_value = self.register_7
			elif value == 'R8':
				new_value = self.register_8
			else:
				raise Exception(f'Unknown value {value} for sub')
			# sub
			new_value = self.sub(x=self.register_1, y=new_value)
			# save
			self.register_1 = self.decimal_to_ieee754(float(new_value))[3]
		elif current_command == 'mult':
			if value == 'R1':
				new_value = self.register_1
			elif value == 'R2':
				new_value = self.register_2
			elif value == 'R3':
				new_value = self.register_3
			elif value == 'R4':
				new_value = self.register_4
			elif value == 'R5':
				new_value = self.register_5
			elif value == 'R6':
				new_value = self.register_6
			elif value == 'R7':
				new_value = self.register_7
			elif value == 'R8':
				new_value = self.register_8
			else:
				raise Exception(f'Unknown value {value} for mult')
			# mult
			new_value = self.mult(x=self.register_1, y=new_value)
			# save
			self.register_1 = self.decimal_to_ieee754(float(new_value))[3]
		elif current_command == 'div':
			if value == 'R1':
				new_value = self.register_1
			elif value == 'R2':
				new_value = self.register_2
			elif value == 'R3':
				new_value = self.register_3
			elif value == 'R4':
				new_value = self.register_4
			elif value == 'R5':
				new_value = self.register_5
			elif value == 'R6':
				new_value = self.register_6
			elif value == 'R7':
				new_value = self.register_7
			elif value == 'R8':
				new_value = self.register_8
			else:
				raise Exception(f'Unknown value {value} for div')
			# div
			new_value = self.div(x=self.register_1, y=new_value)
			# save
			self.register_1 = self.decimal_to_ieee754(float(new_value))[3]
		else:
			raise Exception(f'Unknown command {parts[0]}')
		print(f"Command № {cl // 2 + 1} clock rate {cl + 2}\n"
		      f'Command: {current_command}')
		register_1 = self.decimal_to_ieee754(self.register_1)
		register_2 = self.decimal_to_ieee754(self.register_2)
		register_3 = self.decimal_to_ieee754(self.register_3)
		register_4 = self.decimal_to_ieee754(self.register_4)
		register_5 = self.decimal_to_ieee754(self.register_5)
		register_6 = self.decimal_to_ieee754(self.register_6)
		register_7 = self.decimal_to_ieee754(self.register_7)
		register_8 = self.decimal_to_ieee754(self.register_8)
		print(f"1R: {register_1[0]} {register_1[1]} {register_1[2]}   {self.register_1}\n"
		      f"2R: {register_2[0]} {register_2[1]} {register_2[2]}   {self.register_2}\n"
		      f"3R: {register_3[0]} {register_3[1]} {register_3[2]}   {self.register_3}\n"
		      f"4R: {register_4[0]} {register_4[1]} {register_4[2]}   {self.register_4}\n"
		      f"5R: {register_5[0]} {register_5[1]} {register_5[2]}   {self.register_5}\n"
		      f"6R: {register_6[0]} {register_6[1]} {register_6[2]}   {self.register_6}\n"
		      f"7R: {register_7[0]} {register_7[1]} {register_7[2]}   {self.register_7}\n"
		      f"8R: {register_8[0]} {register_8[1]} {register_8[2]}   {self.register_8}\n"
		      '\n'
		      f"IC: {current_command}")
		print()
	
	def run(self, commands_file=None):
		"""
		simulation of running commands and print data
		:param commands_file: read commands from txt file
		"""
		if not commands_file:  # if we give new commands
			self.load_all_commands(file=self.executing_commands_file)
		else:  # by default
			self.load_all_commands(file=commands_file)
		for c, command in enumerate(self.commands):  # executing commands
			print('_' * 100)
			self.ex_com(current_command=command, cl=2 * c)
		print('_' * 100)
		print('Results:')
		print(f"1R: {self.register_1}\n"
		      f"2R: {self.register_2}\n"
		      f"3R: {self.register_3}\n"
		      f"4R: {self.register_4}\n"
		      f"5R: {self.register_5}\n"
		      f"6R: {self.register_6}\n"
		      f"7R: {self.register_7}\n"
		      f"8R: {self.register_8}\n")
		input()


def main():
	"""
	Start simulation
	"""
	file_commands = os.getcwd() + '/task.txt'
	proc_sim = Processor(commands_file=file_commands)
	proc_sim.run()


if __name__ == '__main__':
	main()
